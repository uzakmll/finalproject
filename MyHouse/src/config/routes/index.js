import React, { Component } from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack'


import { Home, Activity, Inbox, Account, Payment, Food, Login } from '../../page'
const MaterialBottom = createBottomTabNavigator()
const Stack = createStackNavigator()

const HomeStack = () => {
    return (
        <Stack.Navigator
            initialRouteName='Login'
        >
            <Stack.Screen name='Home' component={BootomTabs} 
            options={
                {
                    title: 'Home',
                    headerShown:false
                }
            }
            />
            <Stack.Screen name='Food' component={Food} 
            options = {{ title: 'Food',headerShown:true }}
            />
            
        </Stack.Navigator>
    )
}

const BootomTabs = () => {
    return (
        <MaterialBottom.Navigator
            shifting={false}
            initialRouteName='Home'

        >
            <MaterialBottom.Screen name='Home' component={Home}
                options={{
                    tabBarLabel: 'Home',
                    tabBarIcon: ({ color }) => (
                        <View>
                            <Image source={require('../../assets/icon/nav-home.jpg')} style={{ width: 30, height: 30 }} />
                        </View>
                    )

                }}
            />
            
            <MaterialBottom.Screen name='Inbox' component={Inbox}
                options={{
                    tabBarLabel: 'Log In',
                    tabBarIcon: ({ color }) => (
                        <View>
                            <Image source={require('../../assets/icon/nav-inbox.jpg')} style={{ width: 30, height: 30 }} />
                        </View>
                    ),
                    tabBarBadge: 1

                }}
            />
            
            <MaterialBottom.Screen name='Account' component={Account}
                options={{
                    tabBarLabel: 'Account',
                    tabBarIcon: ({ color }) => (
                        <View>
                            <Image source={require('../../assets/icon/nav-account.jpg')} style={{ width: 30, height: 30 }} />
                        </View>
                    )

                }} />
        </MaterialBottom.Navigator>
    )
}


export default function index() {
    return (
        <NavigationContainer>
            <HomeStack />
        </NavigationContainer>
    )
}

const styles = StyleSheet.create({})
