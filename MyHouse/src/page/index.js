import Home from '../page/Home'
import Activity from '../page/Activity'
import Inbox from '../page/Inbox'
import Account from '../page/Account'
import Payment from '../page/Payment'
import Food from '../page/Food'


export {Login} from '../page/Account/Login'

export {Home, Activity, Inbox, Account,Payment, Food, }
