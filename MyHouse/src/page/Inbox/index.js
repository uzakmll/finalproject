import React, {useState} from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default function Register () {
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isError, setIsError] = useState(false);

    const submit=()=>{
        const Data ={
            username, password
        }
        console.log(Data)

        if(password ==="12345678"){
            setIsError(false)
            console.log("Login Benar")
            navigation.navigate("Home",{
                username : username
            })

        }else{
            console.log("Login Salah")
            setIsError(true)
        }


    }

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.text_header}>Welcome!</Text>
            </View>
            <View style={styles.footer}>
                <Text style={styles.text_footer}>Username</Text>
                <View style={styles.action}>
                    <TextInput 
                    placeholder="Your Username" 
                    style={styles.text_input} 
                    autoCapitalize="none" 
                    value={username}
                    onChangeText={(value)=>setUsername(value)}
                    
                    />
                     
                    

                </View>
                <Text style={styles.text_footer}>Email</Text>
                <View style={styles.action}>
                    <TextInput 
                    placeholder="Your Email" 
                    style={styles.text_input} 
                    autoCapitalize="none" 
                    value={email}
                    onChangeText={(value)=>setEmail(value)}
                    
                    />
                     
                    

                </View>
                <Text style={styles.text_footerTwo}>Password</Text>
                <View style={styles.action}>
                    <TextInput 
                    placeholder="Your Password" 
                    style={styles.text_input} 
                    autoCapitalize="none" 
                    value={password}
                    onChangeText={(value)=>setPassword(value)}
                    
                    />
                    

                </View>
                <View style={styles.buttomSatu}>
                    <TouchableOpacity style={styles.signIn}>
                        <Text style={styles.textSign_satu}>Sign Up</Text>
                    </TouchableOpacity>
                </View> 
                <View style={styles.textButton}>
                    <Text style={styles.textAlready}>Already  have an account ?</Text>   
                    <View style={styles.buttomDua}>    
                        <TouchableOpacity>
                            <Text style={styles.textSign_dua}>Sign In</Text>
                        </TouchableOpacity>
                    </View>    
                </View>
            </View>
            
            
        </View>
    )
}



const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#F4F4F4',
        
        
        
    },
    header:{
        flex: 1,
        justifyContent: 'flex-end',
        paddingHorizontal: 20,
        paddingBottom: 50,
    },
    footer:{
        flex: 3,
        backgroundColor: 'white',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingHorizontal: 28,
        paddingBottom: 10,
    },
    text_header:{
        color: 'black',
        fontSize: 30,
    },
    text_footer:{
        color: 'black',
        fontSize: 18,
        paddingTop: 50,
        marginBottom: -23
        
    },
    text_footerTwo:{
        color: 'black',
        fontSize: 18,
        paddingTop: 30,
        marginBottom: -23
        
    },
    action:{
        flexDirection: 'row',
        marginTop: 30,
        borderBottomWidth: 1,
        borderBottomColor: 'black',
        paddingBottom: 1,
    },
    text_input:{
        flex: 1,
        marginTop: -9,
        paddingLeft: 10,
        color: 'black',
        
        
    },
    buttomSatu:{
        alignItems: 'center',
        marginTop: 40,
        marginBottom: 28,
        backgroundColor: '#09AB54',
        borderRadius: 50,
        paddingTop: 9,
        paddingBottom: 9

    },
    buttomDua:{
        alignItems: 'flex-end',
        marginTop: 8,
        backgroundColor: 'white',
        borderRadius: 50,
        paddingTop: 9,
        paddingBottom: 9,
        

    },
    textAlready:{
        paddingTop: 10,
        color: '#09AB54'
    },
    textSign_satu:{
        fontSize: 18,
        color: 'white'
        
    },
    textSign_dua:{
        fontSize: 15,
        
    },
    textButton:{
        flex: 2,

    }

});