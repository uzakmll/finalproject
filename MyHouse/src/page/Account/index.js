import React from 'react';
import { Text, View, Image, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default function About() {
    return (
        <View style={styles.container}>
            <View style={styles.wrapper}>
                <Text style={styles.containerText}>Account</Text>
                <Image
                    style={styles.avatarLogo} source={require('../../assets/images/profil.png')}
                />
                <View>
                    <Text style={styles.nama}>@gf56467gvgh</Text>
                </View>
            </View>
            <View style={styles.wrapperSatu}>
                <Text style={styles.setting}>Pengaturan</Text>
                <Text style={{color: 'grey',marginLeft: 130}}>Privasi dan Logout</Text>
                <View style={styles.garis}></View>
                <Text style={styles.setting}>Pusat Bantuan</Text>
                <Text style={{color: 'grey',marginLeft: 100}}>Pusat Bantuan dan Keamanan</Text>
                <View style={styles.garis}></View>


            </View>


        </View>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        color: 'white',
        alignItems: 'center',
        

    },
    avatarLogo: {
        height: 150,
        width: 150,
        marginLeft:80,
        borderRadius: 500

    },
    containerText: {
        fontSize: 20,
        marginTop: 20,
        marginBottom: 20,
        marginLeft:120
        
    },
    nama: {
        fontSize: 19,
        marginTop: 18,
        paddingLeft: 99,
    },
    content: {
        fontSize: 19,
        marginTop: 18,
        
    },
    
    wrapper:{
        
        width: 320,
        height: 280,
        marginTop: 50,
        backgroundColor: 'white',
        elevation: 4,
        borderRadius: 10,
    },
    wrapperSatu:{
        
        width: 380,
        height: 280,
        marginTop: 10,
        backgroundColor: 'white',
        elevation: 4,   
    },
    setting:{
        fontSize: 18,
        marginLeft: 140,
        marginTop: 30,
    },
    garis:{
        marginTop: 15,
        borderBottomWidth: 1,
        borderBottomColor: 'black',
    }


})