import React from 'react'
import { StyleSheet, Text, View, Image, Dimensions, ScrollView } from 'react-native'
import OvoComponent from '../../component/OvoComponent'
import FiturUtama from '../../component/FiturUtama'
import PromoItems from '../../component/PromoItems'



const {width} = Dimensions.get('window')

export default function App() {
  return (
    <ScrollView style={{flex: 1, backgroundColor: 'white'}}>
      <Image style={{height: 140, width: '100%'}} source={require('../../assets/images/awan.jpg')}/>
      <Text style={styles.greetingText}>Selamat Siang</Text>
      <View style={styles.wrapperOvo}>
        <View style={styles.textOvo}>
          <Text style={{fontSize: 17, fontWeight: 'bold',color: '#383838'}}>Ovo Balance</Text>
          <Text style={{fontSize: 17, fontWeight: 'bold',color: '#383838'}}>1.000.000</Text>
        </View>
        <View style={styles.garisDiOvo}></View>
        <OvoComponent />
        
      </View>
      <View style={{marginHorizontal:18}}>
        <FiturUtama />
      </View>
      <View style={styles.divider}></View>
      <PromoItems/>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  greetingText:{
    fontSize: 17,
    fontWeight: 'bold',
    position: 'absolute',
    alignSelf: 'center',
    top: 30,
    color: '#383838'
  },
  wrapperOvo:{
    marginHorizontal: 18,
    height: 150,
    marginTop: -60,
    backgroundColor: 'white',
    elevation: 4,
    borderRadius: 10,
  },
  textOvo:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 12,
    marginTop: 10,
  },
  garisDiOvo:{
    height: .8,
    backgroundColor: '#adadad',
    marginTop: 10,
  },
  divider:{
    width: width,
    height: 15,
    backgroundColor: '#ededed',
    marginVertical: 15,
  }
})
