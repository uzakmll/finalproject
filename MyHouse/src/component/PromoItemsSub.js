import React from 'react'
import { StyleSheet, Text, View, Dimensions, Image } from 'react-native'


const {width} = Dimensions.get('window')
const PromoItemsSub = (props) => {
    return (
        <View style={styles.wrapper}>
                <Image source={props.image} style={styles.propsImage}/>
                <View style={styles.textPromo}>
                    <Text style={{fontWeight: 'bold', fontSize: 15}}>{props.text}</Text>
                </View>
                {
                    props.diskon ? <View style={styles.textDiskon}><Text>{props.diskon}</Text></View> :
                    <View></View>
                }
        </View>
    )
}
export default PromoItemsSub;

const styles = StyleSheet.create({
    wrapper:{
        backgroundColor:'white',
        elevation:4,
        borderRadius: 8,
        width: width / 2 - 27,
        marginRight: 18,
        marginBottom: 18,
    },
    propsImage:{
        height: width / 2 -27,
        width: width/ 2 -27,
        borderRadius: 10,
    },
    textPromo:{
        marginLeft:10,
        marginVertical:10,
    },
    textDiskon:{
        position: 'absolute',
        top: 10,
        backgroundColor: 'white',
        padding: 4, 
        borderRadius: 4,
        left: 4,
    }
})
