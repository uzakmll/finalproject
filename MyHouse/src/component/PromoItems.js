import React, { Component } from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import PromoItemsSub from './PromoItemsSub'

const PromoItems= () => {
    return (
        <View style={{marginHorizontal:18, width:'100%', flexWrap: 'wrap', flexDirection: 'row'}}>
            <PromoItemsSub
            image={{uri : 'https://foto.co.id/wp-content/uploads/2016/12/Teknik-foto-makanan-iga.jpg'}}
            text='Sup Buntut Iga Sapi'
            diskon='Diskon 40%'
            />
            <PromoItemsSub
            image={{uri : 'https://cdn.idntimes.com/content-images/post/20190729/66348731-2305110659574744-1633586503236977077-n-9b8d166287d653c929ca8a456da71966.jpg'}}
            text='Baso Jeletot'
            
            />
            <PromoItemsSub
            image={{uri : 'https://www.woke.id/wp-content/uploads/2019/09/sate-nusantara-12-famous-satay-from-indonesia-indoindians-758x500.jpg'}}
            text='Sate Padang'
            diskon='Diskon 20%'
            />
            <PromoItemsSub
            image={{uri : 'https://www.resepistimewa.com/wp-content/uploads/martabak-telur.jpg'}}
            text='Martabak Telor'
            diskon='Diskon 5%'
            />
        </View>
    )
}

export default PromoItems

const styles = StyleSheet.create({})
