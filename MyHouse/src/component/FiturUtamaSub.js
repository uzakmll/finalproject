import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'

const FiturUtamaSub = (props) => {
    return (
        
            <TouchableOpacity onPress={props.onPress} style={{width:'25%', alignItems: 'center'}}>
                <Image style={styles.imageFeatureUtama} source={props.image}/>
                <Text style={styles.textFiturUtama}>{props.title}</Text>
            </TouchableOpacity>
            
           
        
    )
}


export default FiturUtamaSub;
const styles = StyleSheet.create({
    imageFeatureUtama:{
        height: 55,
        width: 55,
        marginTop: 15,
    },
    textFiturUtama:{
        textAlign: 'center',
        marginTop: 4,
    }

})

